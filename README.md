# langages et IDE



Je veux un langage qui soit orientés vers la création d'utilitaires comme le _C_ mais plus _moderne_ ayant la plupart des caratéristiques suivantes:

- générant un code compact,
- ayant des performances équivalentes à celles de _C_,
- _garbage collected_,
- offrant les bases de la programmation fonctionnelle dont le strict minimum serait:
  - les fonctions sont _first class objects_,
  - les *closures*

et ayant un ou plusieurs IDE avec ces caractéristiques:

- émulation _Vim_,
- gestion de _Git_ intégrée,
- possibilité de construction de l'application,
- utilisation de _make_,
- accès au débugger.

Bien entendu, ce langage doit être suffisament mûr et doit disposer d'une bonne docummentation.