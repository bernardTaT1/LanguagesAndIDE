#!/usr/bin/env python
# -*- coding: utf-8

# ======================================================================
# PHexDumpP.py from PHexDumP
# written by bernard
# 19/07/18 22:28
# ======================================================================

import sys
import os.path

version = '1.0.0'


def show_version(program_name):
    print(program_name + " version " + version)
    sys.exit(0)


def show_help(program_name, exit_code):
    print(program_name + " a sample program")
    print(program_name + " [-h|--help]: show this text and exits")
    print(program_name + " -v|--version: show version and exits")
    sys.exit(exit_code)


def main(argn, argv):
    program_name = os.path.basename(argv[0])

    def search_basic_args(args):
        for arg in args:
            if arg == '-h' or arg == '--help':
                show_help(program_name, 0)
            elif arg == '-v' or arg == '--version':
                show_version(program_name)

    if argn == 1:
        show_help(program_name, 0)
    else:
        search_basic_args(argv[1:])

    print('That\'s cool...')


if __name__ == "__main__":
    main(len(sys.argv), sys.argv)
