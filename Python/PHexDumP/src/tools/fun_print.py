#!/usr/bin/env python
# -*- coding: utf-8


def print_strings(strings):
    for s in strings:
        print("  ->" + s + "<-")
