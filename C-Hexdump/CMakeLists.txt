
# CMAKE_HOST_SYSTEM_NAME
message("CMAKE_HOST_SYSTEM_NAME is ${CMAKE_HOST_SYSTEM_NAME}")
if (CMAKE_HOST_SYSTEM_NAME STREQUAL "NetBSD")
cmake_minimum_required(VERSION 3.11)
# TODO: aller rechercher gcc 7 dans /usr/pkg/gcc7/...
set(GCC_BASE "/usr/pkg/gcc7")
set(GCC_BIN "${GCC_BASE}/bin")
set(CMAKE_C_COMPILER "${GCC_BIN}/gcc")
set(CMAKE_C_COMPILER_AR "${GCC_BIN}/gcc-ar")
set(CMAKE_C_COMPILER_RANLIB "${GCC_BIN}/gcc-ranlib")
# set(CMAKE_NM "${GCC_BIN}/gcc-nm")
else()
cmake_minimum_required(VERSION 3.12)
endif()

project(C C)

set(CMAKE_VERBOSE_MAKEFILE false)
set(CMAKE_C_STANDARD 11)

set(CMAKE_INSTALL_PREFIX "/home/bernard")

set(WARN_FLAGS "-Wall -pedantic -Wextra -Wunused -Wundef -Wshadow -Wmissing-noreturn -Winline")
set(MORE_FLAGS "-std=c11")
set(CMAKE_C_FLAGS "${WARN_FLAGS} ${MORE_FLAGS}")

set(SRCS
        main.c
        hexdump.c
        tools/app.c
        tools/app-option.c
        tools/file-reader.c
        tools/hexops.c
        tools/rbuffer.c
        tools/onerrors.c
        )

set(HEADERS
        hexdump.h
        tools/app.h
        tools/app-option.h
        tools/compat.h
        tools/file-reader.h
        tools/hexops.h
        tools/rbuffer.h
        tools/private-file-header.h
        tools/onerrors.h
        )

message("CMAKE_C_FLAGS_DEBUG is ${CMAKE_C_FLAGS_DEBUG}")
message("CMAKE_C_FLAGS is ${CMAKE_C_FLAGS}")

add_executable(C-Hexdump ${SRCS} ${HEADERS})
