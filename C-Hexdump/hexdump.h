//
// Created by bernard on 29/07/18.
//

#ifndef C_HEXDUMP_H
#define C_HEXDUMP_H

#include "tools/compat.h"

#if !defined(_REAL_GCC_)
#error "Works only with gcc"
#endif

#if !defined(IS_C11)
#error "Works only with -std=c11"
#endif


void hexdump(const char *file_name);

#endif //C_HEXDUMP_H
