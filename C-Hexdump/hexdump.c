// ============================================================
// hexdump.c from CLion::C
//
// Created by bernard on 01/08/18 at 23:01.
// ============================================================

#include "hexdump.h"


#include "tools/file-reader.h"
#include "tools/hexops.h"

#define HLEN    16
#define LLEN    256


static uint8_t buffer[HLEN];
static char line[LLEN];

void hexdump(const char* fileName) {
    void *fd = fr_open(fileName, NULL);

    if (fd != NULL) {
        ssize_t read_len;
        while ((read_len = fr_read(fd, buffer, HLEN)) > 0) {
            char *dst = line;
            ssize_t rest = read_len;
            uint8_t *src = buffer;
            uint8_t *old_src = src;
            ssize_t imax = min((ssize_t)HLEN, rest);

            memset(line, (int)' ', LLEN);

            dst += sprintf(dst, "%08lx  ", fr_before_position(fd));
            for (ssize_t i = 0; i < imax; i++) {
                dst = put_hex_byte(dst, *(src++));
                if (i == 7) {
                    dst = put1space(dst);
                }
            }
            if (imax < HLEN) {
                for (ssize_t i = imax; i < HLEN; i++) {
                    dst = put3spaces(dst);
                    if (i == 7) {
                        dst = put1space(dst);
                    }
                }
            }
            src = old_src;
            dst = put1space(dst);
            *(dst++) = '|';
            for (ssize_t i = 0; i < imax; i++) {
                *(dst++) = normalize_byte(*(src++));
            }
            *(dst++) = '|';
            *dst = 0;
            fprintf(stdout, "%s\n", line);
        }
        // BOF : do fprintf BEFORE fr_close, if not, BUG !
        fprintf(stdout, "%08lx\n", fr_before_position(fd));
        fr_close(fd);
    } else {
        exit_on_error("Cannot open file", fileName);
    }
}
