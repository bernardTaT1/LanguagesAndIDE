# le langage C
Ici, le but est surtout de tester _CLion_, l'IDE de Jetbrain pour le langage C.

La première constatation est que _CLion_ utilise _CMake_ et ne semble pas pouvoir utiliser _Make_. Je suis déçu, et, bien que conscient des avantages de _CMake_ par rapport à _GNU Make_, ce dernier me manquera fortement. De même que _BSD make_.

Ceci dit, _CLion_ a la classe des autres IDEs de Jetbrain. Maintenant, reste plus qu'à le configurer, ce qui est assez long mais n'est pas spécifique à _CLion_. Et à l'utiliser.

Je profite de ce projet pour tenter un (tout) petit framework de gestion d'application en ligne de commande.

## premières constatations

### Markdown

Ce n'est pas le meilleur outil pour gérer les fichiers _Markdown_. Il vaut mieux disposer d'un autre éditeur plus spécialisé comme _Typora_ mais on peut déjà travailler avec l'extension _Markdown Navigator 2.0_. Une autre extension existe bien, _Markdown Support_, mais elle n'est pas compatible avec la version actuelle (2018.2) de _CLion_.

À noter que cette extension,  _Markdown Navigator 2.0_, a deux versions, la plus belle étant payante, 15.99 $ par an... Là, je n'achète plus!

### Git

Le support est identique à celui des autres IDE de JetBrain, c'est à dire parfait.

### C

À la création du projet, on choisi entre _C_ et _C++_ et la version du langage utilisée. Pour le _C_, je ne me pose plus de questions existencielles, je prends toujours la norme _C11_.

C'est un vrai IDE, on peut compiler, lancer l'exécution du résultat de compilation ou bien le débugger.

## conclusion temporaire

C'est la plus belle expérience d'IDE pour le C que j'ai depuis... et bien depuis très longtemps, disons 2004 lorsque j'ai découvert _XCode_ d'*Apple*.

Écrit en Java, donc très portable, il est pourtant très rapide, il ne _colle_ pas comme peuvent le faire _Netbeans_ ou _Eclipse_. 

L'émulation _Vim_ est très complète et me fait oublier que je ne suis pas sur _Vim_ lui-même.

Les points négatifs sont:

- trop souvent, la description des plugins ne permet pas de savoir si _C/C++_ sont visés,
- certains plugins ne sont pas compatibles avec _CLion_ ou du moins avec la version dont je dispose et le seul moyen de le savoir est de les installer,
- le prix est un _abonnement_! En fait, je ne possèderais _jamais_ ma version de _CLion_,
- le prix par année est élevé même pour la licence individuelle.

Ce sont les deux derniers points qui m'irritent le plus, pourtant je vais débourser les 89.00 € du prix de la première année d'utilisation.