#!/usr/bin/env bash

# run-test.sh from CLion::C
#
# Created by bernard on 06/08/18 at 00:38.

cmake-build-debug/C-Hexdump README.md > 1.log 
! [[ -f 2.log ]] && hexdump -Cv README.md > 2.log && echo "2.log created"

diff 1.log 2.log
