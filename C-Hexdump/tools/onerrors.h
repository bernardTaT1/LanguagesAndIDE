// ============================================================
// onerrors.h from CLion::C
//
// Created by bernard on 03/08/18 at 08:40.
// ============================================================

#ifndef __onerrors_h__
#define __onerrors_h__

// don't remove const qualifier her, the noreturn attribute
// is strange...
void __exit_on_error(const int error_id,
                        const char *message,
                        const char *object_name)
__attribute__((noreturn));

#define exit_on_error(message, object) \
    __exit_on_error(errno, message, object)

#endif // __onerrors_h__
