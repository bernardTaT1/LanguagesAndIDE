// ============================================================
// onerrors.c from CLion::C
//
// Created by bernard on 03/08/18 at 08:40.
// ============================================================

#include "compat.h"

const int error_buffer_len = 1024;

void __exit_on_error(const int error_id,
        const char *message,
        const char *object_name) {
    char error_buffer[error_buffer_len];
    // TODO: do something when error_id is 0
    if (strerror_r(error_id, error_buffer, error_buffer_len - 1) != 0) {
        sprintf(error_buffer, "Unkown error %d", error_id);
    }
    fprintf(stderr, "ERROR %s: %s -> %s\n", error_buffer, message, object_name);
    exit(-1);
}

