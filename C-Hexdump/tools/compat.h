//
// Created by bernard on 29/07/18.
//

#ifndef C_COMPAT_H
#define C_COMPAT_H

// cf. https://mort.coffee/home/c-compiler-quirks/
#if (__STDC_VERSION__ >= 201112L)
# if defined(__GNUC__) && !defined(__clang__)
#  if (__GNUC__ >= 5 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 9))
#   define IS_C11
#  endif
# else
#  define IS_C11
# endif
#endif

#if defined(__GNUC__) && !defined(__clang__)
#  define _REAL_GCC_
#endif


#ifdef __GNUC__
#  define UNUSED(x) UNUSED_ ## x __attribute__((__unused__))
#else
#  define UNUSED(x) UNUSED_ ## x
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <errno.h>

#include "onerrors.h"

#if !defined(min)
static inline int __min_int(const int x, const int y) {
   if (x < y) {
       return x;
   } else {
       return y;
   }
}
static inline ssize_t __min_ssize_t(const ssize_t x, const ssize_t y) {
    if (x < y) {
        return x;
    } else {
        return y;
    }
}

// cf. http://www.robertgamble.net/2012/01/c11-generic-selections.html
// TODO: pour une meilleure utilisation de _Generic,
//
#define min(X,Y)   _Generic((X), \
    int: __min_int, \
    ssize_t: __min_ssize_t \
)(X,Y)

#endif // !defined(min)

#endif // C_COMPAT_H
