// ============================================================
// app-option.c from CLion::C
//
// Created by bernard on 30/07/18 at 22:59.
// ============================================================

#include "compat.h"

#include "app.h"
#include "app-option.h"

extern TSApp theApp;
extern char *version;

static TEOptionResult cback_help(void *);
static TEOptionResult cback_version(void *);

TSAppOption basic_options[] = {
    {
        .short_option = 'h',
        .long_option = "help",
        .text = "[-h|--help]: show this text and exits",
        // for futur use only
#if 0
        .parameter_number = 0,
        .parameters = NULL,
#endif
        //
        .param_callback = cback_help
    },
    {
        .short_option = 'v',
        .long_option = "version",
        .text = "-v|--version: show current version and exits",
        // for futur use only
#if 0
        .parameter_number = 0,
        .parameters = NULL,
#endif
        //
        .param_callback = cback_version
    },
    {
        .short_option = '\0',
        .long_option = NULL,
        .text = NULL,
        // for futur use only
#if 0
        .parameter_number = 0,
        .parameters = NULL,
#endif
        //
        .param_callback = NULL
    }
};

static TEOptionResult cback_help(void *param) __attribute__((noreturn));
static TEOptionResult cback_help(void *param) {
    TSAppOption *options = (TSAppOption *)param;
    fprintf(stdout, "%s %s\n", theApp.name, theApp.version);
    while (options->text != NULL) {
        fprintf(stdout, "%s %s\n", theApp.name, options->text);
        options++;
    }
    exit(0);
}

static TEOptionResult cback_version(void *UNUSED(param)) __attribute__((noreturn));
static TEOptionResult cback_version(void *UNUSED(param)) {
    fprintf(stdout, "%s version %s\n", theApp.name, theApp.version);
    exit(0);
}

TEOptionResult on_char_option(TSAppOption *options, char option) {
    while (options->text != NULL) {
        if (options->short_option == option) {
            return options->param_callback(options);
        }
        options++;
    }
    return OPTR_IS_NOT_OPTION;
}

TEOptionResult on_string_option(TSAppOption *options, char *option) {
    while (options->text != NULL) {
        if (!strcmp(options->long_option, option)) {
            return options->param_callback(options);
        }
        options++;
    }
    return OPTR_IS_NOT_OPTION;
}
