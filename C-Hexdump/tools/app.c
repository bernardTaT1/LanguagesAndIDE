//
// Created by bernard on 28/07/18.
//

#include "compat.h"

#include <libgen.h>

#include "app.h"
#include "app-option.h"

TSApp theApp;
char *version = "1.0.0";

static void show_help(void);
static void search_basic_args(void);

void init_app(int argn, char **argv, char *UNUSED(more_help)) {
    theApp.version = version;
    theApp.name = basename(argv[0]);
    theApp.argn = argn;
    theApp.argv = argv;
#if 0
    theApp.more_help = more_help;
#endif
    if (argn == 1) {
        show_help();
    } else {
        search_basic_args();
    }
}


static inline void show_help(void) {
    on_char_option(basic_options, 'h');
}

static void search_basic_args(void) {
    for (int i=1; i<theApp.argn; i++) {
        TEOptionType current_type;
        char *ptrOption = theApp.argv[i];

        if (*ptrOption == '-') {
            current_type = OTYPE_SHORT;
            ptrOption++;
            if (*ptrOption == '-') {
                current_type = OTYPE_LONG;
                ptrOption++;
            }
            switch (current_type) {
                case OTYPE_SHORT:
                    on_char_option(basic_options, *ptrOption);
                    break;
                case OTYPE_LONG:
                    on_string_option(basic_options, ptrOption);
                    break;
                case OTYPE_NONE:
                    // it's an error !!!!
                    break;
            }
        }
    }
}
