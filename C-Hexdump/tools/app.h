//
// Created by bernard on 28/07/18.
//

#ifndef C_APP_H
#define C_APP_H

#include <stdbool.h>

typedef struct _TSApp {
    char *version;
    char *name;
    int argn;
    char **argv;

#if 0
    char *more_help;
    bool with_debug;
    bool is_verbose;
#endif
} TSApp;


void init_app(int argn, char **argv, char *more_help);
#endif //C_APP_H
