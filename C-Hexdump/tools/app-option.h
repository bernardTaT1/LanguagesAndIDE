// ============================================================
// app-option.h from CLion::C
//
// Created by bernard on 30/07/18 at 23:00.
// ============================================================

#ifndef __app_option_h__
#define __app_option_h__

typedef enum {
    OPTR_OK,
    OPTR_FAILURE,
    OPTR_IS_NOT_OPTION
} TEOptionResult;

typedef enum {
    OTYPE_NONE,
    OTYPE_SHORT,
    OTYPE_LONG
} TEOptionType;

typedef TEOptionResult (*on_option_params)(void *);

typedef struct _TSAppOption {
    char short_option;
    char *long_option;
    char *text;
    // for futur use only
#if 0
    int parameter_number;
    char **parameters;
#endif
    //
    on_option_params param_callback;

} TSAppOption;

extern TSAppOption basic_options[];

TEOptionResult on_char_option(TSAppOption *options, char option);
TEOptionResult on_string_option(TSAppOption *options, char *option);

#endif // __app_option_h__
