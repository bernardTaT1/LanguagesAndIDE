//
// Created by bernard on 28/07/18.
//

#include "hexdump.h"
#include <sys/stat.h>
#include "tools/app.h"

static char *more_help = "[OPTIONS] file file ...: " \
    "show the hexdump of all the files";

static bool is_dir(char *fileName) {
    struct stat s;

    if (!stat(fileName, &s)) {
        return S_ISDIR(s.st_mode);
    } else {
        exit_on_error("Cannot access file", fileName);
        return false;
    }
}

int main(int argn, char **argv) {
    init_app(argn, argv, more_help);

    for (int i=1; i<argn; i++) {
        if (!is_dir(argv[i])) {
            hexdump(argv[i]);
        } else {
            exit_on_error("Cannot open a directory", argv[i]);
        }
    }
    return 0;
}
