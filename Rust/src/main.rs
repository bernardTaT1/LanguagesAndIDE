use std::env;

fn get_exe_name() -> String {
    let name = env::args().nth(0).unwrap();
    let the_closure = || {
       name
    };
    the_closure()
}

fn dohelp(exit_code: i32) {
    println!("{} [-h|--help]: show this text and exits", get_exe_name());
    println!("{} -v|--version: show version and exits", get_exe_name());
    println!("{} file ... : show hexdumped content of files", get_exe_name());
    std::process::exit(exit_code);
}


fn main() {
    println!("Hello, world! Rust is good for you!");
    for argument in env::args() {
        println!("-> {}", argument);
        // cf. https://mgattozzi.github.io/2016/05/26/how-do-i-str-string.html
        if argument.eq(&"-h".to_string()) | argument.eq(&"--help".to_string()) {
            dohelp(0);
        }
    }
}
